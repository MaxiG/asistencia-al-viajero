var Movil = function(name, color, historyPositions) {
    this.name = name;
	this.color = color;
    this.historyPositions = historyPositions;
    //this.icono = "ferrari.png";
    this.marker = L.marker(historyPositions[0].position, {icon: ferrari});
    var actualIx = 0;

    this.mover = function(callback) {
        var self = this;
        setTimeout(function() {
            callback(historyPositions[actualIx]);

            actualIx += 1;
            if(actualIx < historyPositions.length) {
                self.mover(callback);
            }
        }, 1000);
    }
};

var ferrari = L.icon({
    iconUrl: '../img/ferrari.png',
    iconSize: [40, 20],
    iconAnchor: [, 50],
    popupAnchor: [-3, -76],
    shadowSize: [34, 50],
    shadowAnchor: [15, 60]
});

