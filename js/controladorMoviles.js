var movilesDeAsistencia = function(map){
    var recorrido = new RecorridoMovil("1K", map);
    var lista = listaMoviles();

    for(var element of lista){
        recorrido.agregarMovil(element); 
    }

    recorrido.start();
}

var listaMoviles = function(){
    var listaMoviles = syncQuery("https://assistanceservices.herokuapp.com/api/supporttrucks/",skip());
    listaMoviles = listaMoviles.supportTrucks.map(function(elem){
        var url = "https://assistanceservices.herokuapp.com/api/supporttrucks/" + elem.id + "/positions/";
        var movilPos = syncQuery(url,skip());
        var movil = new Movil(elem.id,'red', movilPos.positions);
        return movil;
    });
    return listaMoviles;
}

var estado = function(int){
    var estado = syncQuery("https://assistanceservices.herokuapp.com/api/truckstates/"+int , skip);
    return estado.state.description;
}



