
//Creación de Mapas.
var createMap = function(nodeId) {
    // Ubicación de la UNGS.
    var ungsLocation = [-34.5221554, -58.7000067];

    // Creación del componente mapa de Leaflet.
    var map = L.map(nodeId).setView(ungsLocation, 14);

    // Agregamos los Layers de OpenStreetMap.
    var baseLayer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    // Agregamos el control para seleccionar Layers al mapa
    var layersControl = L.control.layers({
        "Base": baseLayer
    });
    layersControl.addTo(map);

    return map;
}

////////////////////////////////////////////////////////////////////

function drawAlerts(alerts, url) {   
    var codigo = "";
    var respuesta = alerts;
    if (url !== "https://ws.smn.gob.ar/alerts/"){
        console.log("SI ENTRO");
        respuesta = alerts.alerts;
    }
    respuesta.forEach(function(alert) {
       codigo +=`<div class="informe"><h3>${alert.title}</h3>
                <p>${alert.date}</p>
                <p>${alert.description}</p>
                
                <h4>Zonas Afectadas</h4>
                `
        codigo += "<ul>";
        for(i in alert.zones){
            codigo += "<li>"+alert.zones[i]+"</li>"
        }
        codigo += "</ul></div>";
    });

    $("#alerts").html(codigo);
}

////////////////////////////////////////////////////////////////////

function drawCenters(map) {      
    var centros = syncQuery("../JSON/centros.json", skip());
    var res = '';
    for (elemen of centros){

      //  var centroNuevo = new Centro(elemen.id, elemen.nombre, elemen.horarios,elemen.telefono, elemen.direccion, elemen.posicion);
        marker = L.marker(elemen.posicion);
        marker.addTo(map);
        marker.bindTooltip(elemen.nombre);
    }
}


 function infoCentro(){
     var centros = syncQuery("../JSON/centros.json", skip());

    var res ='';
     for(var i of centros)
     {
         res +=`
            <div id="infoCentro"> 
                <h1>${i.nombre}</h1>
                <p>${i.direccion}</p>
                <p>${i.horariosAtencion}</p>
                <p>${i.telefono}</p>
            </div>
            `;
     }
    $("#info").html(res);
}

