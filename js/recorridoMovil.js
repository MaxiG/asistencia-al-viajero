var RecorridoMovil = function(name, map) {
    this.name = name;
    this.map = map;
    this.moviles = [];

    this.agregarMovil = function(movil) {
            movil.marker.addTo(map);
            var updater = function(newPosition) {
            var pos = newPosition.position;
            var state = estado(newPosition.state);
           
            movil.marker.setLatLng(pos).update();  
            movil.marker.bindTooltip(state);  
        }

        this.moviles.push({
            movil: movil,
            updater: updater
        })
    }

    this.start = function() {
        this.moviles.forEach(function(data) {
            var movil = data.movil;
            movil.mover(data.updater);
        });
    }
};