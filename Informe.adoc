= Prototipo de la Aplicación Asistencia al Turista 

=== Grupo "El Rejunte"

=== Integrantes del Grupo 

Carlos Figueroa <lumusika@gmail.com>

Marcos Vera <marcosgabriel2297@gmail.com>

Maximiliano Gamarra <maxilgstylus@gmail.com>

=== Docentes de la Materia

Daniel ALvarez 

Fedrico Lamuedra

(COM-01)

=== Fecha de Entrega: 26/06/2019

:numbered:
:source-highlighter: highlight.js
:tabsize: 4
//ctrl + shift + p y escribimos asciidoc y preview.

= Introducción

En el presente informe se explicará las decisiones tomadas en el desarrollo del prototipo  de la aplicación Asistencia al Turista​​. Se detallará la funcionalidad incluida para la demostración, su interfaz y se explicará en líneas generales el funcionamiento del código.

= Desarrollo

La vista de la aplicación se desarrolló utilizando código HTML con el objetivo de modelar a grandes rasgos la estructura que podría tener la versión final del sistema. Se implementó un archivo través de css para otorgarle cierto grado de personalización al sitio. La lógica y el comportamiento se implementó por medio de Javascript y con JSON se realizó el tratamiento de la información obtenida de la API.

== Descripción del Código 

El código fuente de la aplicación se encuentra ordenados por carpetas y existe un único archivo html en dónde se importan todos los archivos desarrollados para que la página web pueda funcionar.

=== CSS

Es donde se encuentra el archivo que le otorga una mejor vista a los componentes de la página como así también su distribución en la pantalla, por lo tanto le corresponde la responsabilidad de la vista dentro del proyecto web.

=== IMG (Imágenes)

En esta carpeta almacenamos los diferentes archivos de imágenes que se utilizarán tanto para el fondo como para los móviles. 

=== JS (Archivos de Javascript)

Es en dónde se encuentran los archivos que le otorgan lógica y comportamiento a la aplicación. Se compompone de:

* En la *carpeta objetos* se encuentran *alerta.js*, *centros.js* y *Movil.js* donde se muestra las definiciones de tales entidades. Además en el último se agrega  el método mover, comportamiento que debe saber realizar tal objeto.

* En *lectores.JSON* se tiene a *getJson.js* encargado de la definición de los dos tipos de consultas a la API para obtener las alertas y los móviles. Para el primer caso, se desarrollo un pedido asincrónico y para el segundo, uno sincrónico.

[source, js]

----

//Pedido Sincrónico para Móviles y Centros

var syncQuery = function(url, callback) {

    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", url, false);
    xhttp.send();

    if (xhttp.status === 200) {
        resObj = JSON.parse(xhttp.responseText)
        return resObj;
    }
    return null;
}
var skip = function(response) {
    };
////////////////////////////////////////////////////////////////////////

//Pedido Asincrónico para Alertas
  
var asyncQuery = function(url, callback) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        // https://stackoverflow.com/questions/13293617/why-is-my-ajax-function-calling-the-callback-multiple-times
        if (this.readyState === 4) {
            if (this.status === 200) {
                // parseamos el resultado para obtener el objeto JavaScript
                resObj = JSON.parse(xhttp.responseText)
                // llamamos a la función callback con el objeto parseado como parámetro.
                callback(resObj);
            }
        }
    };
    xhttp.open("GET", url, true);
    var ret = xhttp.send();
    return ret;
}
----

*consultorAlertas.js* contiene la función de consultar las alertas a la API de forma asincrónica.

[source, js]
----

function consultarAlertas(){

    var callback = function(response) {
       
        if(response){
          drawAlerts(response, urlClima);
        }else{
          console.log("Error en el callback al procesar los datos. No se recibieron datos?");
          };
        }
      
        asyncQuery(urlClima, callback);
}
----

*controladorMoviles.js* comprende las siguientes funciones: _listaMoviles_ encargada de transformar los pedidos sincrónicos a la APi en variables de tipo listas, tanto para los móviles como para las posiciones por las que circularán. _movilesDeAsistencia_ se encarga de iniciar de forma sincronizada el recorrido para los móviles utilizando los valores que retorna la función listaMoviles. _estado_ retorna el estado de los móviles en base a la consulta sincrónica a la API.

[source, js]
----
var movilesDeAsistencia = function(map){
    var recorrido = new RecorridoMovil("1K", map);
    var lista = listaMoviles();

    for(var element of lista){
        recorrido.agregarMovil(element); 
    }

    recorrido.start();
}

var listaMoviles = function(){
    var listaMoviles = syncQuery("https://assistanceservices.herokuapp.com/api/supporttrucks/",skip());
    listaMoviles = listaMoviles.supportTrucks.map(function(elem){
        var url = "https://assistanceservices.herokuapp.com/api/supporttrucks/" + elem.id + "/positions/";
        var movilPos = syncQuery(url,skip());
        var movil = new Movil(elem.id,'red', movilPos.positions);
        return movil;
    });
    return listaMoviles;
}

var estado = function(int){
    var estado = syncQuery("https://assistanceservices.herokuapp.com/api/truckstates/"+int , skip);
    return estado.state.description;
}
----

*recorridoMovil.js* se encarga de armar el recorrido para los moviles en base al conjunto de posiciones que posee cada uno y a través de la función anónima _updater_ se van actualizando las posiciones y se da forma al movimiento de los móviles.

[source, js]
----
var RecorridoMovil = function(name, map) {
    this.name = name;
    this.map = map;
    this.moviles = [];

    this.agregarMovil = function(movil) {
            movil.marker.addTo(map);
            var updater = function(newPosition) {
            var pos = newPosition.position;
            var state = estado(newPosition.state);
           
            movil.marker.setLatLng(pos).update();  
            movil.marker.bindTooltip(state);  
        }

        this.moviles.push({
            movil: movil,
            updater: updater
        })
    }

    this.start = function() {
        this.moviles.forEach(function(data) {
            var movil = data.movil;
            movil.mover(data.updater);
        });
    }
};
----

*drawers.js* este archivo contiene la lógica para crear el mapa en la página a través de la función _createMap_. Con _drawAlerts_ se maneja la visualización de los datos correspondientes a las alertas inyectando código html. El _drawCenters_ se encarga de obtener los datos de los centros a través de una consulta sincrónica a un archivo Json, luego dibujarlos en el mapa y, mediante la librería leaflet, con el _bindTooltip_ mostrar el nombre de los centros cuando el mouse pasa por encima de alguno de ellos. _infoCentro_ permite tratar la información de los centros que se muestra en la página. 

[source, go]
----
//Creación de Mapas.
var createMap = function(nodeId) {
    // Ubicación de la UNGS.
    var ungsLocation = [-34.5221554, -58.7000067];

    // Creación del componente mapa de Leaflet.
    var map = L.map(nodeId).setView(ungsLocation, 14);

    // Agregamos los Layers de OpenStreetMap.
    var baseLayer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    // Agregamos el control para seleccionar Layers al mapa
    var layersControl = L.control.layers({
        "Base": baseLayer
    });
    layersControl.addTo(map);

    return map;
}

////////////////////////////////////////////////////////////////////

function drawAlerts(alerts, url) {   
    var codigo = "";
    var respuesta = alerts;
    if (url !== "https://ws.smn.gob.ar/alerts/"){
        console.log("SI ENTRO");
        respuesta = alerts.alerts;
    }
    respuesta.forEach(function(alert) {
       codigo +=`<div class="informe"><h3>${alert.title}</h3>
                <p>${alert.date}</p>
                <p>${alert.description}</p>
                
                <h4>Zonas Afectadas</h4>
                `
        codigo += "<ul>";
        for(i in alert.zones){
            codigo += "<li>"+alert.zones[i]+"</li>"
        }
        codigo += "</ul></div>";
    });

    $("#alerts").html(codigo);
}

////////////////////////////////////////////////////////////////////

function drawCenters(map) {      
    var centros = syncQuery("../JSON/centros.json", skip());
    var res = '';
    for (elemen of centros){

      //  var centroNuevo = new Centro(elemen.id, elemen.nombre, elemen.horarios,elemen.telefono, elemen.direccion, elemen.posicion);
        marker = L.marker(elemen.posicion);
        marker.addTo(map);
        marker.bindTooltip(elemen.nombre);
    }
}


 function infoCentro(){
     var centros = syncQuery("../JSON/centros.json", skip());

    var res ='';
     for(var i of centros)
     {
         res +=`
            <div id="infoCentro"> 
                <h1>${i.nombre}</h1>
                <p>${i.direccion}</p>
                <p>${i.horariosAtencion}</p>
                <p>${i.telefono}</p>
            </div>
            `;
     }
    $("#info").html(res);
}
----

*config.js* contiene las diferentes url que permiten conectar con las diferentes APIs para obtener información. Desde este archivo se puede modificar el servicio de alerta requerido cambiando el valor de la variable _urlClima_. Si este es nacional corresponde al Servicio Meteorológico Nacional y si apunta a urlProfe la información llega desde assistanceservices.

[source, go]
----
var heroku = "https://assistanceservices.herokuapp.com/api";
var nacional = "https://ws.smn.gob.ar/alerts/";
var local = "http://localhost:3000/api/";

var dia = "0";
var urlProfe = heroku + "/alerts/day/"+dia;
var urlClima = nacional;


var urlStates = heroku + "/truckstates/";
var urlTrucks = heroku + "/supporttrucks/";


var Config = {
    urlClima,
    urlStates,
    urlTrucks
}
----

*bootstrap.js* es la función principal desde donde se inicia la aplicación. Contiene una variable que recibe la representación del mapa creado por la función _createMap_. Luego realiza el llamado a los diferentes métodos ya descritos tales como: _consultarAlertas_, _movilesDeAsistencia_, _drawCenters_, y _infoCentro_ para brindar la información a mostrar en la página web.  

[source, go]
----
var bootstrap = function(){  
  
  var map = createMap('mapid');
  
    consultarAlertas();
    movilesDeAsistencia(map);
    drawCenters(map);
    infoCentro();
}

$(bootstrap);
----

= Conclusiones

El desarrollo del prototipo representó un desafío importante. Si bien intentamos aplicar lo aprendido en clase, al no estar familiarizados con los lenguajes de programación utilizados: javascript, HTML y CSS, el tiempo de desarrollo se vio reducido debido a la necesidad de adquirir conocimientos. Por otro lado, al no contar con una base firme en las tecnologías, nos centramos en cubrir los requerimientos básicos para el funcionamiento de la misma en detrimento de la performance, la confiabilidad y el diseño del software ya que lo importante era cumplir con la funcionalidad requerida. Como primer acercamiento a las tecnologías web este proyecto resultó muy productivo.
